# Emulator design
## Structure

```mermaid
graph LR
subgraph Backends
BECH8[Chip-8]
end

BECH8[Chip-8] ---|Interface| Middleware

Middleware ---|Interface| UI
```

## Components & Component Interfaces
### CHIP-8 Backend
- CPU registers
    - 16 General purpose
        - type: address/value
    - index
        - type: address
    - delay timer value
        - type: timer
    - sound timer value
        - type: timer
    - program counter
        - type: address
    - stack pointer
        - type: address
- timers
    - sound
        - decrements each iterpretation step
    - delay
        - decrements each iterpretation step
- instructions
    - 36 total
    - [reference](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM)
- memory
    - 4kb, 512b reserved for interpreter
        - type: array of int (address/value/instruction)
    - stack: 64 byte
        - array of address
- framebuffer
    - specs
        - monochrome
        - 64 x 32 mode
    - implementation
        - in memory: 2D array of bool
- sound
    - on / off
        - type: boolean flag
    - on while sound timer >= 0

## Middleware / Host
- features
    - instantiates backend (CHIP-8) instances
    - translates backend calls to API calls
    - serves Websocket API
    - receives requests from client, responds to them
    - pauses/resumes emulator execution as needed
- implementation
    - backend
        - interface
            - Provides access to state in the backend
            - functions
                - Step()
                    - execute one instruction
                - StepN(n int)
                    - execute n instructions
                - Run()
                    - execute all instructions
                - Pause()
                    - pause execution
                - Reset()
                    - reset the state of execution
                - Load(game string) 
                    - load a game 
                    - predefined options
                    - use built-in assets or load from disk?
                - Sound() bool
                    - get sound state
                    - for CHIP-8: boolean
                - Frame() FrameBuffer
                    - get the current framebuffer state
                - KeyDown(k rune)
                    - send a key down event to the backend
                - KeyUp(k rune)
                    - send a key up event to the backend
                - CurrentState()
                  - sends current values of:
                    - registers
                    - cur
    
## User interface
- features
    - screen
    - actions 
        - choose game from preselected list
    - options
        - rescaling method
            - 4x
        - target framerate (60)
        - volume
    - debugger
      - show current machine state
        - registers

## Implementation Details

