# Research
## CHIP-8
### Articles / Discussions
- https://old.reddit.com/r/EmuDev/comments/d5q7y0/book_to_read_if_i_want_to_learn_about_creating/
- http://blog.alexanderdickson.com/javascript-chip-8-emulator
- http://devernay.free.fr/hacks/chip8/C8TECH10.HTM
- http://devernay.free.fr/hacks/chip8/C8TECH10.HTM
- http://emubook.emulation64.com/
- http://emulator101.com/
- http://mattmik.com/files/chip8/mastering/chip8.html
- http://stevelosh.com/blog/2016/12/chip8-cpu/
- http://www.codeslinger.co.uk/
- http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/
- http://www.pong-story.com/chip8/
- https://austinmorlan.com/posts/chip8_emulator/
- https://aymanbagabas.com/2018/09/17/chip-8-emulator.html
- https://github.com/Lonami/chip8-asm64-emu
- https://gyani.net/blog/chip-8/
- https://medium.com/average-coder/exploring-emulation-in-go-chip-8-636f99683f2a
- https://walterkuppens.com/post/writing-a-chip-8-virtual-machine-in-rust/
- https://www.lexaloffle.com/bbs/?tid=2492
- https://engineering.wpengine.com/building-8-bit-emulator-in-golang/
- https://blog.scottlogic.com/2017/12/13/chip8-emulator-webassembly-rust.html
- https://gyani.net/blog/chip-8/
- https://deokhwankim.wordpress.com/2019/09/29/1/
### Emulator Projects
- https://github.com/Monkeyanator/chipm8
- https://github.com/ejholmes/chip8
- https://github.com/h4ck3rk3y/go-8
- https://github.com/jamesmcm/chip8go
- https://github.com/justinmoor/CHIP-8
- https://github.com/massung/chip-8
- https://github.com/skatiyar/go-chip8
- https://github.com/whisper0077/chip-8-golang
- https://github.com/zachabrahams/gochip8
- https://massung.github.io/CHIP-8/
#### Rust
- https://medium.com/@bokuweb17/writing-an-nes-emulator-with-rust-and-webassembly-d64de101c49d
- https://blog.scottlogic.com/2017/12/13/chip8-emulator-webassembly-rust.html
- https://github.com/rodrigorc/raze
- 
### Assemblers
- https://github.com/JohnEarnest/Octo

