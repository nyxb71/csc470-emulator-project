# User Stories
## Epics
- As a user, I should be able to play CHIP-8 games using the emulator.
    - As a user, I should be able to use the emulator on my computer.
    - As a user, I should be able to choose which game to play.
    - As a user, I should be able to use the keyboard to play the game.
- As a power user, I should be able to use the emulator debugger, so I can see how the emulator works.
    - As a power user, I should be able to view the current state of the emulator.
