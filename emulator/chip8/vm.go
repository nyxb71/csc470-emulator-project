package chip8

import (
	"fmt"
	"math/rand"
	"time"
)

// These types are here to differentiate the interpretation
// of instruction arguments and opcode components.

type Address uint16 // actually uses 12 least significant bits
func address(a uint16) Address {
	if a > 0xFFF {
		fmt.Printf("WARNING: lossy conversion into Address\n")
	}

	return Address(a & 0xFFF)
}
func (a *Address) Set(new Address) {
	*a = address(uint16(new))
}

type Opcode uint16
type Register byte
type Value byte
type Memory byte
type Timer uint8

type Keycode uint8
type Keyboard [16]bool

type Pixel uint8 // Either 1 or 0, but use uint8 for XOR

const (
	Maxmem       = 4096
	numRegisters = 16
	stackDepth   = 16
)

// Gets copied to memory starting at 0x50
var fontset = []Memory{
	0xF0, 0x90, 0x90, 0x90, 0xF0, // 0 -> 0x50
	0x20, 0x60, 0x20, 0x20, 0x70, // 1 -> 0x55
	0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2 -> 0x5A
	0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3 -> 0x5F
	0x90, 0x90, 0xF0, 0x10, 0x10, // 4 -> 0x64
	0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5 -> 0x69
	0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6 -> 0x6E
	0xF0, 0x10, 0x20, 0x40, 0x40, // 7 -> 0x73
	0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8 -> 0x78
	0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9 -> 0x7D
	0xF0, 0x90, 0xF0, 0x90, 0x90, // A -> 0x82
	0xE0, 0x90, 0xE0, 0x90, 0xE0, // B -> 0x87
	0xF0, 0x80, 0x80, 0x80, 0xF0, // C -> 0x8C
	0xE0, 0x90, 0x90, 0x90, 0xE0, // D -> 0x91
	0xF0, 0x80, 0xF0, 0x80, 0xF0, // E -> 0x96
	0xF0, 0x80, 0xF0, 0x80, 0x80} // F -> 0x9B

type VM struct {
	// Entry point for the program
	base Address
	// Program counter
	pc Address
	// Stack
	stack [stackDepth]Address // 64 bytes
	// Stack pointer
	sp Address
	// Regular registers (V0,V1,...,VF)
	v [numRegisters]Register
	// Index register
	i Address
	// Memory
	ram [Maxmem]Memory
	rom [Maxmem]Memory

	// Framebuffer (display)
	fbrows uint8
	fbcols uint8
	fb     [][]Pixel // 32x64, or 64x128

	// Sound timer
	st Timer
	// Delay timer
	dt Timer

	// Status of keys
	keys Keyboard
	// Which keys are we waiting for?
	keywait Keycode
}

func NewVM(fr *ROM) VM {
	// Initialize registers
	registers := [numRegisters]Register{}
	for i := 0; i < numRegisters; i++ {
		registers[i] = 0
	}

	var rom [Maxmem]Memory
	for i := 0; i < fr.Length(); i++ {
		rom[fr.Base+Address(i)] = Memory(fr.Contents[i])
	}

	for i := 0; i < len(fontset); i++ {
		rom[0x50+i] = fontset[i]
	}

	var ram [Maxmem]Memory
	copy(ram[:], rom[:])

	// Initialize framebuffer
	framebuffer := make([][]Pixel, fr.FBRows)
	for i := 0; i < len(framebuffer); i++ {
		framebuffer[i] = make([]Pixel, fr.FBCols)
	}

	return VM{
		base:   fr.Base,
		pc:     fr.Base, // programs usually start at the base (0x200)
		stack:  [stackDepth]Address{},
		sp:     0,
		v:      registers,
		i:      0,
		ram:    ram,
		rom:    rom,
		fbrows: fr.FBRows,
		fbcols: fr.FBCols,
		fb:     framebuffer,
		st:     0,
		dt:     0,
	}
}

func (vm *VM) Reset() {
	vm.pc = vm.base
	vm.sp = 0

	vm.stack = [stackDepth]Address{}

	vm.v = [numRegisters]Register{}
	vm.i = 0

	copy(vm.ram[:], vm.rom[:])

	vm.cls()

	vm.st = 0
	vm.dt = 0
}

func (vm *VM) LoadROM(rom *ROM) {
	vm.base = rom.Base
	for i := 0; i < rom.Length(); i++ {
		vm.rom[rom.Base+Address(i)] = Memory(rom.Contents[i])
	}
	vm.Reset()
}

func (vm *VM) KeyDown(key Keycode) error {
	if key >= 16 {
		return fmt.Errorf("invalid key")
	}

	vm.keys[key] = true

	return nil
}

func (vm *VM) KeyUp(key Keycode) error {
	if key >= 16 {
		return fmt.Errorf("invalid key")
	}

	vm.keys[key] = false

	return nil
}

func (vm *VM) resetkeys() {
	vm.keys = Keyboard{}
}

func (vm *VM) fetch() Opcode {
	i := vm.pc

	// Since opcodes are two bytes wide,
	// get two byes from memory (at the current pc) and combine them
	vm.pc += 2
	return Opcode(uint16(vm.ram[i])<<8 | uint16(vm.ram[i+1]))
}

func (vm *VM) Step() {
	vm.dispatch(vm.fetch())

	if vm.dt > 0 {
		vm.dt--
	}

	if vm.st > 0 {
		vm.st--
	}
}

type State struct {
	PC    Address                // program counter
	Stack [stackDepth]Address    // 64 bytes
	SP    Address                // stack pointer
	V     [numRegisters]Register // registers V0-VF
	I     Address                // Index register

	RAM [Maxmem]Memory

	FB [][]Pixel // Framebuffer

	ST Timer // Sound Timer
	DT Timer // Delay Timer
}

func (vm *VM) CurrentState() State {
	return State{
		PC:    vm.pc,
		Stack: vm.stack,
		SP:    vm.sp,
		V:     vm.v,
		I:     vm.i,

		RAM: vm.ram,

		FB: vm.fb,

		ST: vm.st,
		DT: vm.dt,
	}
}

type KeyEvent struct {
	KeyDown bool
	Key     Keycode
}

func (vm *VM) HandleKeyEvent(event KeyEvent) {
	if event.KeyDown {
		vm.KeyDown(event.Key)
	} else {
		vm.KeyUp(event.Key)
	}
}

func (vm *VM) Run(fps int, vmState chan State, keyEvents chan KeyEvent) {
	for {
		// Export the current frame to the host
		vmState <- vm.CurrentState()

		time.Sleep(time.Second / time.Duration(fps))

		// If a key was pressed, handle it
		select {
		case event, ok := <-keyEvents:
			if ok {
				vm.HandleKeyEvent(event)
			}
		// If no event was in the channel, continue execution
		default:
			break
		}

		vm.Step()
		vm.resetkeys()
	}
}

func (vm *VM) dispatch(op Opcode) {
	nibble1 := op & 0x000F
	nibble2 := (op & 0x00F0) >> 4
	nibble3 := (op & 0x0F00) >> 8
	nibble4 := (op & 0xF000) >> 12
	byte1 := op & 0x00FF
	nibbles1to3 := op & 0x0FFF

	switch nibble4 {
	case 0x0:
		switch byte1 {
		case 0xE0: // 0x00E0
			vm.cls()
		case 0xEE: // 0x00EE
			vm.ret()
		case 0xFB: // 0x00FB
			vm.scright()
		case 0xFC: // 0x00FC
			vm.scleft()
		case 0xFE: // 0x00FE
			vm.low()
		case 0xFF: // 0x00FF
			vm.high()
		default: // 0x00Cn
			vm.scdown(Value(nibble1))
		}

	case 0x1: // 0x1nnn
		vm.jmp(Address(nibbles1to3))
	case 0x2: // 0x2nnn
		vm.call(Address(nibbles1to3))
	case 0x3: // 0x3xkk
		vm.skeq1(Register(nibble3), Value(byte1))
	case 0x4: // 0x4xkk
		vm.skne1(Register(nibble3), Value(byte1))
	case 0x5: // 0x5xy0
		vm.skeq2(Register(nibble3), Register(nibble2))
	case 0x6: // 0x6xkk
		vm.mov(Register(nibble3), Value(byte1))
	case 0x7: // 0x7xkk
		vm.add(Register(nibble3), Value(byte1))
	case 0x8:
		switch nibble1 {
		case 0x0: // 0x8xy0
			vm.movreg(Register(nibble3), Register(nibble2))
		case 0x1: // 0x8xy1
			vm.or(Register(nibble3), Register(nibble2))
		case 0x2: // 0x8xy2
			vm.and(Register(nibble3), Register(nibble2))
		case 0x3: // 0x8xy3
			vm.xor(Register(nibble3), Register(nibble2))
		case 0x4: // 0x8xy4
			vm.addreg(Register(nibble3), Register(nibble2))
		case 0x5: // 0x8xy5
			vm.subl(Register(nibble3), Register(nibble2))
		case 0x6: // 0x8x06
			vm.shr(Register(nibble3))
		case 0x7: // 0x8xY7
			vm.subr(Register(nibble3), Register(nibble2))
		case 0xE: // 0x8x0E
			vm.shl(Register(nibble3))
		}
	case 0x9: // 0x9xy0
		vm.skne2(Register(nibble3), Register(nibble2))
	case 0xA: // 0xAnnn
		vm.movi(Address(nibbles1to3))
	case 0xB: // 0xBnnn
		vm.jmpi(Address(nibbles1to3))
	case 0xC: // 0xCxkk
		vm.rand(Register(nibble3), Value(byte1))
	case 0xD: // 0xDxyn
		vm.sprite(Register(nibble3), Register(nibble2), Value(nibble1))
		// 0xDry0
		// xsprite(value(nibble3), value(nibble2))
	case 0xE:
		if byte1 == 0x9E { // 0xEx9E
			vm.skp(Register(nibble3))
		} else { // 0xExA1
			vm.sknp(Register(nibble3))
		}
	case 0xF:
		switch byte1 {
		case 0x07: // 0xFx07
			vm.gdelay(Register(nibble3))
		case 0x0A: // 0xFx0A
			vm.key(Register(nibble3))
		case 0x15: // 0xFx15
			vm.sdelay(Register(nibble3))
		case 0x18: // 0xFx18
			vm.ssound(Register(nibble3))
		case 0x1E: // 0xFx1E
			vm.addi(Register(nibble3))
		case 0x29: // 0xFx29
			vm.font(Register(nibble3))
		case 0x30: // 0xFx30
			vm.xfont(Register(nibble3))
		case 0x33: // 0xFx33
			vm.bcd(Register(nibble3))
		case 0x55: // 0xFx55
			vm.stora(Register(nibble3))
		case 0x65: // 0xFx65
			vm.loada(Register(nibble3))
		}
	default:
		panic("didn't parse opcode")
	}
}

// -------------------- INSTRUCTIONS --------------------

// --- ASSIGNMENT ---
// 0x6xkk MOV (constant)
func (vm *VM) mov(x Register, val Value) {
	vm.v[x] = Register(val)
}

// 0x8xy0 MOV (register)
func (vm *VM) movreg(x Register, y Register) {
	vm.v[x] = vm.v[y]
}

// --- ARITHMETIC ---
// 0x7xkk ADD (constant)
func (vm *VM) add(x Register, val Value) {
	vm.v[x] += Register(val)
}

// 0x8xy4 ADD (register)
func (vm *VM) addreg(x Register, y Register) {
	vm.v[x] += vm.v[y]
}

// 0x8xy5 SUB (register)
func (vm *VM) subl(x Register, y Register) {
	if vm.v[x] > vm.v[y] {
		vm.v[0xF] = 1
	} else {
		vm.v[0xF] = 0
	}

	vm.v[x] -= vm.v[y]
}

// 0x8xy7 SUB (register)
func (vm *VM) subr(x Register, y Register) {
	vm.subl(y, x)
}

// BITWISE OPERATIONS
// 0x8xy1 Bitwise OR
func (vm *VM) or(x Register, y Register) {
	vm.v[x] |= vm.v[y]
}

// 0x8xy2 Bitwise AND
func (vm *VM) and(x Register, y Register) {
	vm.v[x] &= vm.v[y]
}

// 0x8xy3 Bitwise XOR
func (vm *VM) xor(x Register, y Register) {
	vm.v[x] ^= vm.v[y]
}

// 0x8xyE Bitwise Shift Left
func (vm *VM) shl(x Register) {
	// Set the cakky
	vm.v[0xF] = vm.v[x] >> 7
	// Shift left
	vm.v[x] <<= 1
}

// 0x8xy6 Bitwise Shift Right
func (vm *VM) shr(x Register) {
	// Store the LSB in VF
	vm.v[0xF] = vm.v[x] & 1
	// Shift right
	vm.v[x] >>= 1
}

// 0xCxkk Generate a random number
func (vm *VM) rand(x Register, mask Value) {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	// Generate a random uint32 (fast), then reduce it to [0,255]
	// Finally AND it with the provided mask and store the result in Vx
	vm.v[x] = Register(uint8(r.Uint32()&0xFF) & uint8(mask))
}

// --- REGISTER MANAGEMENT ---
// 0xFx65 Load registers from memory
func (vm *VM) loada(reg Register) {
	for i := range vm.v[0 : reg+1] {
		vm.v[i] = Register(vm.ram[vm.i+Address(i)])
	}
	vm.i = vm.i + Address(reg) + 1
}

// 0xFx55 Store registers to memory
func (vm *VM) stora(reg Register) {
	for i, val := range vm.v[0 : reg+1] {
		vm.ram[vm.i+Address(i)] = Memory(val)
	}
	vm.i = vm.i + Address(reg) + 1
}

// --- FLOW CONTROL ---
// 0x3xkk Skip Next If Equal
func (vm *VM) skeq1(x Register, val Value) {
	if vm.v[x] == Register(val) {
		vm.pc += 2
	}
}

// 0x4xkk Skip Next If Not Equal
func (vm *VM) skne1(x Register, val Value) {
	if vm.v[x] != Register(val) {
		vm.pc += 2
	}
}

// 0x5xy0 Skip Next If Equal
func (vm *VM) skeq2(x Register, y Register) {
	if vm.v[x] == vm.v[y] {
		vm.pc += 2
	}
}

// 0x9xy0 Skip Next If Not Equal
func (vm *VM) skne2(x Register, y Register) {
	if vm.v[x] != vm.v[y] {
		vm.pc += 2
	}
}

// 0xBnnn Jump to Address + Offset in V0
func (vm *VM) jmpi(addr Address) {
	vm.pc = Address(vm.v[0]) + addr
}

// 0x1nnn Jump to Address
func (vm *VM) jmp(addr Address) {
	vm.pc = addr
}

// 0x2nnn Call subroutine
func (vm *VM) call(addr Address) {
	if int(vm.sp) >= len(vm.stack) {
		panic("stack overflow")
	}

	vm.stack[vm.sp] = vm.pc
	vm.sp++
	vm.pc = addr
}

// 0x00EE Return from Sub-routine
func (vm *VM) ret() {
	if vm.sp == 0 {
		panic("stack underflow")
	}

	vm.sp--
	vm.pc = vm.stack[vm.sp]
}

// --- KEYPRESSES ---
// 0xEx9E Skip Next If Pressed
func (vm *VM) skp(key Register) {
	if vm.keys[Keycode(vm.v[key])] {
		vm.pc += 2
	}
}

// 0xExA1 Skip Next If Not Pressed
func (vm *VM) sknp(key Register) {
	if !vm.keys[Keycode(vm.v[key])] {
		vm.pc += 2
	}
}

// 0xFx0A Wait for Keypress
func (vm *VM) key(reg Register) {
	// TODO
}

// --- INDEX REGISTER ---
// 0xFx1E ADD to Index Register
func (vm *VM) addi(reg Register) {
	vm.i += Address(vm.v[reg])
}

// 0xAnnn MOV to Index Register
func (vm *VM) movi(val Address) {
	vm.i = val
}

func splitDigits(val uint8) []uint8 {
	digits := make([]uint8, 3)

	for i := 2; i >= 0; i-- {
		digits[i] = val % 10
		val /= 10
	}

	return digits
}

// 0xFx33
func (vm *VM) bcd(reg Register) {
	digits := splitDigits(uint8(vm.v[reg]))
	vm.ram[vm.i] = Memory(digits[0])   // hundreds
	vm.ram[vm.i+1] = Memory(digits[1]) // tens
	vm.ram[vm.i+2] = Memory(digits[2]) // ones
}

// --- FRAMEBUFFER ---
// 0x00E0 Clear Screen
func (vm *VM) cls() {
	for row := uint8(0); row < vm.fbrows; row++ {
		for col := uint8(0); col < vm.fbcols; col++ {
			vm.fb[row][col] = 0
		}
	}
}

// --- GRAPHICS ---
// 0xFx29 Load font
//
// Set I to the memory address of the sprite data corresponding to the
// hexadecimal digit stored in register VX
//
// VX == 0 -> I = 0x50
// VX == 1 -> I = 0x55
// ...
// VX == F -> I = 0x9B
func (vm *VM) font(reg Register) {
	vm.i = Address(0x50 + (vm.v[reg] * 5))
}

// Unpack a byte into an array of bits/Pixels
func byteToPixels(b byte) []Pixel {
	pixels := make([]Pixel, 8)
	for i := 0; i < 8; i++ {
		pixels[7-i] = Pixel((b >> i) & 1)
	}
	return pixels
}

// 0xDxyn
//
// The interpreter reads n bytes from memory, starting at the address stored in
// I. These bytes are then displayed as sprites on screen at coordinates (Vx,
// Vy). Sprites are XORed onto the existing screen. If this causes any pixels to
// be erased, VF is set to 1, otherwise it is set to 0. If the sprite is
// positioned so part of it is outside the coordinates of the display, it wraps
// around to the opposite side of the screen.
//
// https://chip8.fandom.com/wiki/Instruction_Draw
func (vm *VM) sprite(x Register, y Register, height Value) {
	// reset collision flag
	vm.v[0xF] = 0

	// Get the sprite from vm.ram[vm.i]
	h := int(height)
	sprite := make([]Memory, h)
	for row := range sprite {
		sprite[row] = vm.ram[int(vm.i)+row]
	}

	// Each byte is a row of the sprite
	// For each byte, split it into an array of Pixels
	// Starting at (VX,VY), draw the sprite's pixels
	for yOffset, b := range sprite {
		for xOffset, newPixel := range byteToPixels(byte(b)) {
			sx := xOffset + int(vm.v[x])
			sy := yOffset + int(vm.v[y])

			// Only draw pixels that are on the screen
			if sy < int(vm.fbrows) && sx < int(vm.fbcols) {

				oldPixel := vm.fb[sy][sx]
				// If the pixel changed state
				if oldPixel^newPixel == 1 {
					vm.v[0xF] = 1
				}

				// XOR the new pixel onto the current one
				vm.fb[sy][sx] ^= newPixel
			}

		}
	}
}

// --- TIMERS ---
// 0xFx07 Get Delay Timer
func (vm *VM) gdelay(reg Register) {
	vm.v[reg] = Register(vm.dt)
}

// 0xFx15 Set Delay Timer
func (vm *VM) sdelay(reg Register) {
	vm.dt = Timer(vm.v[reg])
}

// 0xFx18 Set Sound Timer
func (vm *VM) ssound(reg Register) {
	vm.st = Timer(vm.v[reg])
}

// --- SUPERCHIP ---
// 0x00FE Set Low Mode
func (vm *VM) low() {
	// TODO
	fmt.Printf("NOT IMPLEMENTED (LOW)\n")
}

// 0x00FF Set High Mode
func (vm *VM) high() {
	// TODO
	fmt.Printf("NOT IMPLEMENTED (HIGH)\n")
}

// 0x00Cn Scroll Down
func (vm *VM) scdown(lines Value) {
	// TODO
	fmt.Printf("NOT IMPLEMENTED (SCDOWN)\n")
}

// 0x00FB Scroll Right
func (vm *VM) scright() {
	// TODO
	fmt.Printf("NOT IMPLEMENTED (SCRIGHT)\n")
}

// 0x00FC Scroll Left
func (vm *VM) scleft() {
	// TODO
	fmt.Printf("NOT IMPLEMENTED (SCLEFT)\n")
}

// 0xFx30
func (vm *VM) xfont(reg Register) {
	// TODO
	fmt.Printf("NOT IMPLEMENTED (XFONT)\n")
}

// 0xDxy0
func (vm *VM) xsprite(rx Value, ry Value) {
	// TODO
	fmt.Printf("NOT IMPLEMENTED (XSPRITE)\n")
}
