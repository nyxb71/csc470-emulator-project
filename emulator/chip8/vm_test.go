package chip8

import (
	"fmt"
	"os"
	"testing"
)

func Setup() VM {
	pong, err := LoadROM("../assets/games/PONG")
	if err != nil {
		fmt.Printf("%s\n", err)
		os.Exit(1)
	}

	return NewVM(&pong)
}

func TestCLS(t *testing.T) {
	vm := Setup()

	// Turn on even rows
	for row := uint8(0); row < vm.fbrows; row++ {
		for col := uint8(0); col < vm.fbcols; col++ {
			if row%2 == 0 {
				vm.fb[row][col] = 1
			}
		}
	}

	vm.cls()

	for row := uint8(0); row < vm.fbrows; row++ {
		for col := uint8(0); col < vm.fbcols; col++ {
			if vm.fb[row][col] != 0 {
				t.Errorf(
					"cls instruction failed, did not properly clear framebuffer\n"+
						"pixel at (%d,%d) was still on", row, col)
			}
		}
	}
}

func TestJMP(t *testing.T) {
	vm := Setup()
	before := vm.pc
	target := Address(0x300) // arbitrary address
	vm.jmp(target)
	if vm.pc != target {
		t.Errorf(
			"jmp instruction failed, did not set pc correctly\n"+
				"wanted %d\n   got %d",
			before, vm.pc)
	}
}

func TestFontSet(t *testing.T) {
	vm := Setup()

	for i := range fontset {
		if vm.ram[0x50+i] != fontset[i] {
			t.Errorf("fontset was not loaded correctly, error at: %04X", 0x50+i)
			return
		}
	}
}

func TestFONT(t *testing.T) {
	vm := Setup()

	for val := 0; val < 16; val++ {
		vm.v[0] = Register(val)
		vm.font(0)

		want := Address(0x50 + (val * 5))
		if vm.i != want {
			t.Errorf("font: index register had incorrect value\n"+
				"wanted: %04X\n   got: %04X", want, vm.i)
			return
		}
	}
}

func TestSPRITE(t *testing.T) {
	vm := Setup()

	vm.i = 0x50 // start of 0 sprite

	vm.sprite(0, 0, 5) // draw a 0 sprite at (0,0)

	shouldBe := [][]Pixel{
		byteToPixels(0xF0)[0:4],
		byteToPixels(0x90)[0:4],
		byteToPixels(0x90)[0:4],
		byteToPixels(0x90)[0:4],
		byteToPixels(0xF0)[0:4],
	}

	for i, row := range shouldBe {
		for j, col := range row {
			if vm.fb[i][j] != col {
				t.Errorf("sprite: failed to draw correctly\n"+
					"expected %d at (%d,%d), got %d", col, i, j, vm.fb[i][j])
				return
			}
		}
	}
}

func TestBCD(t *testing.T) {
	vm := Setup()

	var cases = []struct {
		input uint8
		want  []uint8
	}{
		{input: 0, want: []uint8{0, 0, 0}},
		{input: 9, want: []uint8{0, 0, 9}},
		{input: 10, want: []uint8{0, 1, 0}},
		{input: 99, want: []uint8{0, 9, 9}},
		{input: 100, want: []uint8{1, 0, 0}},
		{input: 123, want: []uint8{1, 2, 3}},
		{input: 254, want: []uint8{2, 5, 4}},
	}

	for _, testcase := range cases {
		t.Run(fmt.Sprintf("%v", testcase),
			func(t *testing.T) {
				vm.v[0] = Register(testcase.input)
				base := Address(0x300)
				vm.i = base
				vm.bcd(0)

				result := vm.ram[base : base+3]
				for i, val := range result {
					if val != Memory(testcase.want[i]) {
						t.Errorf("bcd: wrong result\nwanted: %v\n   got: %v\n",
							testcase.want, result)
					}
				}
			})
	}
}

func TestADDI(t *testing.T) {
	vm := Setup()

	for val := 0; val <= 0xFF; val++ {
		vm.i = 0
		vm.v[0] = Register(val)
		vm.addi(0)

		want := Address(0 + val)
		if vm.i != want {
			t.Errorf("addi: incorrect value\nwanted: %02X\n   got: %02X", want, vm.i)
			return
		}
	}
}

func TestJMPI(t *testing.T) {
	vm := Setup()

	vm.pc = 0x300
	vm.v[0] = 0xFF
	vm.jmpi(0x456)

	want := Address(0xFF + 0x456)
	if vm.pc != want {
		t.Errorf("jmpi: incorrect value\nwanted: %02X\n   got: %02X", want, vm.pc)
		return
	}
}

func TestSTORA(t *testing.T) {
	vm := Setup()

	base := Address(0x200)
	old := [16]Register{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
	vm.v = old    // Load values into registers
	vm.i = base   // Store the registers starting at base
	vm.stora(0xF) // Store all the registers

	for i, val := range old {
		if vm.ram[base+Address(i)] != Memory(val) {
			t.Errorf("stora: incorrect value\nwanted: %v\n   got: %v",
				old, vm.ram[base:base+16])
			return
		}
	}

}

func TestLOADA(t *testing.T) {
	vm := Setup()

	base := Address(0x200)
	storedRegs := [16]Register{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
	for i := 0; i < 16; i++ {
		vm.ram[base+Address(i)] = Memory(storedRegs[i])
	}
	vm.i = base   // Load all the registers starting at base
	vm.loada(0xF) // Load all the registers

	for i, val := range storedRegs {
		if vm.v[Register(i)] != val {
			t.Errorf("stora: incorrect value\nwanted: %v\n   got: %v",
				storedRegs, vm.ram[base:base+16])
			return
		}
	}

}

// -------------------- HELPER FUNCTIONS --------------------

func TestSplitDigits(t *testing.T) {
	var cases = []struct {
		input uint8
		want  []uint8
	}{
		{input: 0, want: []uint8{0, 0, 0}},
		{input: 9, want: []uint8{0, 0, 9}},
		{input: 10, want: []uint8{0, 1, 0}},
		{input: 99, want: []uint8{0, 9, 9}},
		{input: 100, want: []uint8{1, 0, 0}},
		{input: 123, want: []uint8{1, 2, 3}},
		{input: 254, want: []uint8{2, 5, 4}},
	}

	for _, testcase := range cases {
		t.Run(fmt.Sprintf("%v", testcase),
			func(t *testing.T) {
				output := splitDigits(testcase.input)
				for i := range output {
					if output[i] != testcase.want[i] {
						t.Errorf(
							"digits not split correctly\n"+
								"wanted: %d\n   got: %d",
							testcase.want, output)
					}
				}
			})
	}

}

func TestByteToPixels(t *testing.T) {
	var cases = []struct {
		input uint8
		want  []Pixel
	}{
		{input: 0, want: []Pixel{0, 0, 0, 0, 0, 0, 0, 0}},
		{input: 1, want: []Pixel{0, 0, 0, 0, 0, 0, 0, 1}},
		{input: 51, want: []Pixel{0, 0, 1, 1, 0, 0, 1, 1}},
		{input: 85, want: []Pixel{0, 1, 0, 1, 0, 1, 0, 1}},
		{input: 170, want: []Pixel{1, 0, 1, 0, 1, 0, 1, 0}},
		{input: 204, want: []Pixel{1, 1, 0, 0, 1, 1, 0, 0}},
		{input: 254, want: []Pixel{1, 1, 1, 1, 1, 1, 1, 0}},
		{input: 255, want: []Pixel{1, 1, 1, 1, 1, 1, 1, 1}},
	}

	for _, testcase := range cases {
		t.Run(fmt.Sprintf("%v", testcase),
			func(t *testing.T) {
				output := byteToPixels(testcase.input)
				if len(output) != len(testcase.want) {
					t.Errorf(
						"digits not split correctly\n"+
							"input: %d\n"+
							"wanted: %d\n   got: %d",
						testcase.input, testcase.want, output)
					return
				}
				for i := range output {
					if output[i] != testcase.want[i] {
						t.Errorf(
							"digits not split correctly\n"+
								"input: %d\n"+
								"wanted: %d\n   got: %d",
							testcase.input, testcase.want, output)
						break

					}
				}
			})
	}

}
