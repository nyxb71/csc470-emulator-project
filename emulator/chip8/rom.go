package chip8

import (
	"fmt"
	"os"
)

type ROM struct {
	Name     string
	Base     Address
	Contents []byte
	FBRows   uint8
	FBCols   uint8
}

func (r *ROM) Length() int {
	return len(r.Contents)
}

func (r *ROM) Print() {
	for _, b := range r.Contents {
		fmt.Printf("0x%02X\n", b)
	}
	fmt.Printf("ROM LEN %d\n", r.Length())
}

func LoadROM(fp string) (ROM, error) {
	file, err := os.OpenFile(fp, os.O_RDONLY, 0777)
	if err != nil {
		return ROM{}, err
	}
	defer file.Close()

	fileInfo, err := file.Stat()
	if err != nil {
		return ROM{}, err
	}

	// 4096 total ROM space, with some reserved for the interpreter
	if fileInfo.Size() > (4096 - 512) {
		return ROM{}, err
	}

	buf := make([]byte, fileInfo.Size())
	if _, err := file.Read(buf); err != nil {
		return ROM{}, err
	}

	return ROM{Name: fileInfo.Name(), Base: 0x200, Contents: buf, FBRows: 32, FBCols: 64}, nil
}
