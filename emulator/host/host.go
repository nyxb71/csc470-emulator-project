package host

import (
	"emulator/chip8"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/speaker"
	"github.com/faiface/beep/wav"
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
	"github.com/faiface/pixel/pixelgl"
	"github.com/faiface/pixel/text"
	"golang.org/x/image/colornames"
	"golang.org/x/image/font/basicfont"
)

func buttonToKeyEvent(b pixelgl.Button, keyDown bool) (chip8.KeyEvent, error) {
	keycodes := map[pixelgl.Button]chip8.Keycode{
		pixelgl.Key1: 0x1, // '1'
		pixelgl.Key2: 0x2, // '2'
		pixelgl.Key3: 0x3, // '3'
		pixelgl.Key4: 0xC, // '4'
		pixelgl.KeyQ: 0x4, // 'Q'
		pixelgl.KeyW: 0x5, // 'W'
		pixelgl.KeyE: 0x6, // 'E'
		pixelgl.KeyR: 0xD, // 'R'
		pixelgl.KeyA: 0x7, // 'A'
		pixelgl.KeyS: 0x8, // 'S'
		pixelgl.KeyD: 0x9, // 'D'
		pixelgl.KeyF: 0xE, // 'F'
		pixelgl.KeyZ: 0xA, // 'Z'
		pixelgl.KeyX: 0x0, // 'X'
		pixelgl.KeyC: 0xB, // 'C'
		pixelgl.KeyV: 0xF, // 'V'
	}

	kc, ok := keycodes[b]
	if !ok {
		return chip8.KeyEvent{}, fmt.Errorf("unable to convert rune '%c'", b)
	}
	return chip8.KeyEvent{Key: kc, KeyDown: keyDown}, nil
}

func StartVM(rom *chip8.ROM) (*chip8.VM, chan chip8.State, chan chip8.KeyEvent) {

	vm := chip8.NewVM(rom)
	vmState := make(chan chip8.State)
	keyEvents := make(chan chip8.KeyEvent)
	// The VM runs on another goroutine and feeds the state channel
	// It also recieves keypresses from the keyEvents channel
	go vm.Run(60, vmState, keyEvents)

	return &vm, vmState, keyEvents
}

type Host struct {
	window       *pixelgl.Window
	screenHeight int
	screenWidth  int

	roms            []chip8.ROM
	currentROMIndex int

	shouldBeMuted bool
	beepBuffer    *beep.Buffer

	vm          *chip8.VM
	vmState     chan chip8.State
	vmKeyEvents chan chip8.KeyEvent
}

func NewHost() Host {
	screenHeight := 320
	screenWidth := 640
	defaultAssetsPath := "assets/"

	windowConfig := pixelgl.WindowConfig{
		Title:  "Chip-8",
		Bounds: pixel.R(0, 0, float64(screenWidth+400), float64(screenHeight+65)),
		VSync:  true,
	}

	window, err := pixelgl.NewWindow(windowConfig)
	if err != nil {
		panic(err)
	}

	romsList, err := filepath.Glob(defaultAssetsPath + "games/*")
	if err != nil {
		panic(err)
	}

	roms := []chip8.ROM{}
	for _, path := range romsList {
		rom, err := chip8.LoadROM(path)
		if err != nil {
			panic(err)
		}
		roms = append(roms, rom)
	}

	currentROMIndex := 0

	// Start with PONG
	for i, rom := range roms {
		if rom.Name == "PONG" {
			currentROMIndex = i
		}
	}

	f, err := os.Open(defaultAssetsPath + "wav/beep-02.wav")
	if err != nil {
		log.Fatal(err)
	}

	streamer, format, err := wav.Decode(f)
	if err != nil {
		log.Fatal(err)
	}

	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))

	buffer := beep.NewBuffer(format)
	buffer.Append(streamer)
	streamer.Close()

	vm, stateCh, keyEvents := StartVM(&roms[currentROMIndex])

	return Host{
		window:          window,
		screenHeight:    screenHeight,
		screenWidth:     screenWidth,
		shouldBeMuted:   false,
		beepBuffer:      buffer,
		roms:            roms,
		currentROMIndex: currentROMIndex,
		vm:              vm,
		vmState:         stateCh,
		vmKeyEvents:     keyEvents}
}

func (h *Host) currentROM() *chip8.ROM {
	return &h.roms[h.currentROMIndex]
}

func (h *Host) changeROM(index int) {
	fmt.Printf("LOADING: %s\n", h.roms[h.currentROMIndex].Name)
	h.vm.LoadROM(&h.roms[h.currentROMIndex])
}

func (h *Host) nextRom() {
	if h.currentROMIndex < len(h.roms)-1 {
		h.currentROMIndex++
	} else {
		h.currentROMIndex = 0
	}

	h.changeROM(h.currentROMIndex)
}

func getMuteVal(mute bool) string {
	if mute {
		return "UNMUTE"
	} else {
		return "MUTE"
	}
}

func (h *Host) mute() {
	h.shouldBeMuted = true
}

func (h *Host) unmute() {
	h.shouldBeMuted = false
}

func (h *Host) beep(st chip8.Timer) {
	if st != 0 && !h.shouldBeMuted {
		beep := h.beepBuffer.Streamer(0, h.beepBuffer.Len())
		speaker.Play(beep)
	}
}
func (h *Host) updateWindow(curState chip8.State) {
	h.window.Clear(colornames.Black)

	imd := imdraw.New(nil)
	imd.Color = pixel.RGB(0, 255, 0)

	//draw frames
	width, height := float64(h.screenWidth/64), float64(h.screenHeight/32)
	for x := 0; x < len(curState.FB); x++ {
		for y := 0; y < len(curState.FB[0]); y++ {
			if curState.FB[x][y] != 0 {
				imd.Push(pixel.V(width*float64(y), float64(h.screenHeight)-(height*float64(x))))
				imd.Push(pixel.V(width*float64(y)+float64(width), float64(h.screenHeight)-(height*float64(x)+height)))
				imd.Rectangle(0)
			}
		}
	}

	//draw divider
	for y := 0; y < int(h.window.Bounds().H()); y++ {
		imd.Push(pixel.V(float64(h.screenWidth+1), float64(5)*float64(y)))
		imd.Push(pixel.V(float64(h.screenWidth+1)+float64(5), float64(5)*float64(y)+float64(5)))
		imd.Rectangle(0)
	}

	//draw debug info
	basicAtlas := text.NewAtlas(basicfont.Face7x13, text.ASCII)
	basicInfo := text.New(pixel.V(float64(h.screenWidth+20), float64(h.window.Bounds().H()-20)), basicAtlas)
	fmt.Fprintln(basicInfo, "PC: ", curState.PC)
	fmt.Fprintln(basicInfo, "I: ", curState.I)
	for x := 0; x < len(curState.V); x++ {
		fmt.Fprintf(basicInfo, "V%d: %d\n", x, curState.V[x])
	}

	//draw ram
	ram := text.New(pixel.V(float64(h.screenWidth+200), float64(h.window.Bounds().H()-20)), basicAtlas)
	fmt.Fprintf(ram, "0xcurGame")

	//draw rom menu item
	roms := text.New(pixel.V(15, h.window.Bounds().H()-40), basicAtlas)
	fmt.Fprintln(roms, h.currentROM().Name)

	//draw sound menu item
	mute := text.New(pixel.V(290, h.window.Bounds().H()-40), basicAtlas)
	fmt.Fprintln(mute, getMuteVal(h.shouldBeMuted))

	//draw menu box
	for x := 0; x < h.screenWidth; x++ {
		imd.Push(pixel.V(float64(x), h.window.Bounds().H()-10))
		imd.Push(pixel.V(float64(x)+5, h.window.Bounds().H()+5))
		imd.Rectangle(0)
	}
	for x := 0; x < h.screenWidth; x++ {
		imd.Push(pixel.V(float64(x), (h.window.Bounds().H()-55)-5))
		imd.Push(pixel.V(float64(x)+float64(5), (h.window.Bounds().H()-55)+5))
		imd.Rectangle(0)
	}

	for y := 0; y < 55; y++ {
		imd.Push(pixel.V(0, h.window.Bounds().H()-float64(y)))
		imd.Push(pixel.V(5, h.window.Bounds().H()-float64(y)+5))
		imd.Rectangle(0)
	}

	//push to screen
	basicInfo.Draw(h.window, pixel.IM)
	roms.Draw(h.window, pixel.IM.Scaled(roms.Orig, 2))
	mute.Draw(h.window, pixel.IM.Scaled(roms.Orig, 2))
	imd.Draw(h.window)
	h.window.Update()
}

func (h *Host) handleButtonPress() {
	if h.window.JustPressed(pixelgl.MouseButtonLeft) {
		x, y := h.window.MousePosition().X, h.window.MousePosition().Y
		if x > 20 && x < 75 {
			if y > 350 && y < 370 {
				h.nextRom()
			}
		}
	}

	for _, button := range []pixelgl.Button{
		pixelgl.Key1,
		pixelgl.Key2,
		pixelgl.Key3,
		pixelgl.Key4,
		pixelgl.KeyQ,
		pixelgl.KeyW,
		pixelgl.KeyE,
		pixelgl.KeyR,
		pixelgl.KeyA,
		pixelgl.KeyS,
		pixelgl.KeyD,
		pixelgl.KeyF,
		pixelgl.KeyZ,
		pixelgl.KeyX,
		pixelgl.KeyC,
		pixelgl.KeyV,
	} {
		if h.window.Pressed(button) {
			kev, _ := buttonToKeyEvent(button, true)
			h.vmKeyEvents <- kev
		}
	}
}

func (h *Host) Run() {
	for !h.window.Closed() {
		curState := <-h.vmState
		h.beep(curState.ST)
		h.handleButtonPress()
		h.updateWindow(curState)
	}
}

func StartHost() {
	host := NewHost()
	host.Run()
}
