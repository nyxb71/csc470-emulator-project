#!/usr/bin/env bash

echo "--------------------------------------------------------------------------------"
golangci-lint -E dogsled,gocritic,godox,golint,gosec,gosimple,govet,lll,maligned,misspell,prealloc,stylecheck,gocyclo,unconvert,staticcheck -e stutters run ./... 
echo "--------------------------------------------------------------------------------"
