package main

import (
	"emulator/host"

	"github.com/faiface/pixel/pixelgl"
)

func main() {
	pixelgl.Run(host.StartHost)
}
