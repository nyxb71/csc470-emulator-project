# Emulator Backend
## Requirements
### Go

go version 1.13+

Check that you are running a current version with:

    $ go version

### Dependencies
#### ALSA

ALSA is required for sound.
https://github.com/hajimehoshi/oto

Fedora:

    $ dnf install alsa-lib-devel

Ubuntu/Debian:

    $ apt install libasound2-dev

#### PixelGL

https://github.com/faiface/pixel#requirements

#### Go Libraries

    $ go get
    
## Run the emulator

    $ go run main.go
    
## Run tests
    
    $ go test -v ./...

## CHIP8 References

- http://devernay.free.fr/hacks/chip8/C8TECH10.HTM
- http://mattmik.com/files/chip8/mastering/chip8.html
- https://en.wikipedia.org/wiki/CHIP-8
- https://web.archive.org/web/20070502010345/https://www.pdc.kth.se/~lfo/chip8/CHIP8.htm
- http://www.emulator101.com/introduction-to-chip-8.html
